package com.kurylchyk;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class Application {

    private static Candy candy = new Candy();


    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {

        //using DOM
        /*DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("C:\\Users\\werty\\IdeaProjects\\task14_xml\\src\\main\\resources\\Candies.xml"));
        Element element = document.getDocumentElement();
        getDOM(element.getChildNodes());
        System.out.println(candy);*/



        //using SAX

       /* SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            SAXParser saxParser = saxParserFactory.newSAXParser();
            MyHandler handler = new MyHandler();
            saxParser.parse(new File("C:\\Users\\werty\\IdeaProjects\\task14_xml\\src\\main\\resources\\Candies.xml"), handler);
            List<Candy> candyList = handler.getCandyList();

            for(Candy element : candyList)
                System.out.println(element);*/




        //using Stax

        String filename = "C:\\Users\\werty\\IdeaProjects\\task14_xml\\src\\main\\resources\\Candies.xml";
        List<Candy> candyList = StaxXMLReader.parseXML(filename);
        for (Candy element : candyList) {
            System.out.println(element.toString());
        }

    }


    public static void getDOM(NodeList nodeList) {
        Text text;
        for (int index = 0; index < nodeList.getLength(); index++) {
            if (nodeList.item(index) instanceof Element) {
                if (((Element) nodeList.item(index)).getTagName() == "name") {
                    text = (Text) nodeList.item(index).getFirstChild();
                    candy.setName(text.getData().trim());
                }
                if (((Element) nodeList.item(index)).getTagName() == "energy") {
                    text = (Text) nodeList.item(index).getFirstChild();
                    candy.setEnergy(Double.parseDouble(text.getData().trim()));
                }
                if (((Element) nodeList.item(index)).getTagName() == "type") {
                    text = (Text) nodeList.item(index).getFirstChild();
                    candy.setType(text.getData().trim());
                }

                if (((Element) nodeList.item(index)).getTagName() == "water") {
                    text = (Text) nodeList.item(index).getFirstChild();
                    candy.setWater(Integer.parseInt(text.getData().trim()));
                }

                if (((Element) nodeList.item(index)).getTagName() == "sugar") {
                    text = (Text) nodeList.item(index).getFirstChild();
                    candy.setSugar(Integer.parseInt(text.getData().trim()));
                }

                if (((Element) nodeList.item(index)).getTagName() == "fructose") {
                    text = (Text) nodeList.item(index).getFirstChild();
                    candy.setFructose(Integer.parseInt(text.getData().trim()));
                }


                if (((Element) nodeList.item(index)).getTagName() == "vanillin") {
                    text = (Text) nodeList.item(index).getFirstChild();
                    candy.setVanillin(Integer.parseInt(text.getData().trim()));
                }

                if (((Element) nodeList.item(index)).getTagName() == "fat") {
                    text = (Text) nodeList.item(index).getFirstChild();
                    candy.setFat(Double.parseDouble(text.getData().trim()));
                }

                if (((Element) nodeList.item(index)).getTagName() == "protein") {
                    text = (Text) nodeList.item(index).getFirstChild();
                    candy.setProtein(Double.parseDouble(text.getData().trim()));
                }

                if (((Element) nodeList.item(index)).getTagName() == "carbohydrate") {
                    text = (Text) nodeList.item(index).getFirstChild();
                    candy.setCarbohydrate(Double.parseDouble(text.getData().trim()));
                }
                if (((Element) nodeList.item(index)).getTagName() == "production") {
                    text = (Text) nodeList.item(index).getFirstChild();
                    candy.setProduction(text.getData().trim());
                }
                if (nodeList.item(index).hasChildNodes()) {
                    getDOM(nodeList.item(index).getChildNodes());
                }
            }
        }
    }

}
