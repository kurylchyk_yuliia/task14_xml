package com.kurylchyk;

public class Candy {
    private String name;
    private double energy;
    private String type;
    private Ingredients ingredients;
    private Value value;
    private String production;

    Candy() {
        ingredients = new Ingredients();
        value = new Value();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setWater(int water){
        ingredients.setWater(water);
    }
    public void setSugar(int sugar) {
        ingredients.setSugar(sugar);
    }

    public void setFructose(int fructose) {
        ingredients.setFructose(fructose);
    }

    public void setVanillin(int vanillin) {
        ingredients.setVanillin(vanillin);
    }


    public void setFat(double fat) {
        value.setFat( fat);
    }

    public void setProtein(double protein) {
        value.setProtein(protein);
    }

    public void setCarbohydrate(double carbohydrate) {
        value.setCarbohydrate(carbohydrate);
    }

    public void setProduction(String production){
        this.production = production;
    }
   public String toString(){
        return "Name: "+ name+"\n" +
                "Energy: "+energy+"\n"+
                "Type: "+type +"\n"+
                "Ingredients: " + ingredients +"\n"+
                "Value: "+value+"\n"+
                "Production: "+production+"\n";
   }
}
