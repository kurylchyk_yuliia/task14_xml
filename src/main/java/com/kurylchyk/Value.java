package com.kurylchyk;

public class Value {

    private double fat;
    private double protein;
    private double carbohydrate;

    public void setFat(double fat) {
        this.fat = fat;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public void setCarbohydrate(double carbohydrate) {
        this.carbohydrate = carbohydrate;
    }

    public String toString(){
        return "fat "+fat +" protein "+protein+" carbohydrate "+carbohydrate;
    }
}
