package com.kurylchyk;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;



public class StaxXMLReader {


    public static List<Candy> parseXML(String filename) {

        List<Candy> candyList = new ArrayList<>();
        Candy candy = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(filename));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    if (startElement.getName().getLocalPart().equals("cd")) {
                        candy = new Candy();

                    } else if (startElement.getName().getLocalPart().equals("name")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        candy.setName(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals("energy")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        candy.setEnergy(Double.parseDouble(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals("type")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        candy.setType(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals("water")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        candy.setWater(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals("sugar")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        candy.setSugar(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals("fructose")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        candy.setFructose(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals("vanillin")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        candy.setVanillin(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals("fat")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        candy.setFat(Double.parseDouble(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals("protein")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        candy.setProtein(Double.parseDouble(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals("carbohydrate")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        candy.setCarbohydrate(Double.parseDouble(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals("production")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        candy.setProduction(xmlEvent.asCharacters().getData());
                    }
                }

                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("cd")) {
                        candyList.add(candy);
                    }
                }

            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return candyList;
    }
}
