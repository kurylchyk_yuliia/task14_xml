package com.kurylchyk;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class MyHandler extends DefaultHandler {

    private List<Candy> candyList = null;
    private Candy candy = null;
    private StringBuilder data = null;

    public List<Candy> getCandyList(){
        return candyList;
    }

    boolean bName = false;
    boolean bEnergy = false;
    boolean bType = false;
    boolean bWater = false;
    boolean bSugar = false;
    boolean bFructose = false;
    boolean bVanillin = false;
    boolean bFat = false;
    boolean bProtein = false;
    boolean bCarbohydrate = false;
    boolean bProduction= false;


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

    if(qName.equals("cd")){
        candy = new Candy();
        if(candyList  == null) {
            candyList = new ArrayList<Candy>();
        }
        } else if(qName.equals("name")){
            bName = true;
        } else if(qName.equals("energy")){
            bEnergy = true;
        } else if(qName.equals("type")){
            bType = true;
        } else if(qName.equals("water")){
            bWater = true;
        } else if(qName.equals("sugar")){
            bSugar = true;
        } else if(qName.equals("fructose")){
            bFructose = true;
        } else if(qName.equals("vanillin")){
            bVanillin = true;
        } else if(qName.equals("fat")){
            bFat = true;
        } else if(qName.equals("protein")){
            bProtein = true;
        } else if(qName.equals("carbohydrate")){
            bCarbohydrate = true;
        } else if(qName.equals("production")){
            bProduction = true;
        }

        data = new StringBuilder();
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(bName) {
            candy.setName(data.toString());
            bName = false;
        } else if(bEnergy) {
            candy.setEnergy(Double.parseDouble(data.toString()));
            bEnergy = false;
        } else if(bType) {
            candy.setType(data.toString());
            bType = false;
        } else if(bWater) {
            candy.setWater(Integer.parseInt(data.toString()));
            bWater = false;
        } else if(bSugar) {
            candy.setSugar(Integer.parseInt(data.toString()));
            bSugar = false;
        } else if(bFructose) {
            candy.setFructose(Integer.parseInt(data.toString()));
            bFructose = false;
        } else if(bVanillin) {
            candy.setVanillin(Integer.parseInt(data.toString()));
            bVanillin = false;
        } else if(bFat) {
            candy.setFat(Double.parseDouble(data.toString()));
            bFat = false;
        } else if(bProtein) {
            candy.setProtein(Double.parseDouble(data.toString()));
            bProtein = false;
        } else if(bCarbohydrate) {
            candy.setCarbohydrate(Double.parseDouble(data.toString()));
            bCarbohydrate = false;
        } else if(bProduction) {
            candy.setProduction(data.toString());
            bProduction = false;
        }

        if(qName.equals("cd")){
            candyList.add(candy);
        }
    }
    public void characters(char ch[], int start, int length) throws SAXException {
        data.append(new String(ch, start, length));
    }
}
