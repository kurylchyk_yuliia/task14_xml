<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2 style="margin-left: 45%">Candies</h2>
                <table style="font-family:Arial; font-size:14pt; background-color:#bac7a7; width:100%; border:12px double #272727; " >
                    <tr style="margin-right: 1%">
                        <th>Name</th>
                        <th>Energy</th>
                        <th>Type</th>
                        <th>Ingredients</th>
                        <th>Value</th>
                        <th>Production</th>
                    </tr>
                    <xsl:for-each select="Candies/cd">
                        <xsl:sort select="name"/>
                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td>
                                <xsl:value-of select="energy"/>
                            </td>
                            <td>
                                <xsl:value-of select="type"/>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td> <p>Water :    <xsl:value-of select="ingredients/water"/> </p></td>
                                    </tr>
                            <tr>
                                        <td> <p>Sugar :   <xsl:value-of select="ingredients/sugar"/></p></td>
                            </tr>
                            <tr>
                                        <td> <p>Fructose :   <xsl:value-of select="ingredients/fructose"/></p></td>
                            </tr>
                            <tr>
                                        <td> <p>Vanillin :  <xsl:value-of select="ingredients/vanillin"/></p></td>
                            </tr>
                                </table>

                            </td>
                            <td>

                                <table>
                                    <tr>
                                        <td> <p>Fat:    <xsl:value-of select="value/fat"/> </p></td>
                                    </tr>
                                    <tr>
                                        <td> <p>Protein :   <xsl:value-of select="value/protein"/></p></td>
                                    </tr>
                                    <tr>
                                        <td> <p> carbohydrate :   <xsl:value-of select="value/carbohydrate"/></p></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <xsl:value-of select="production"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>

            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>